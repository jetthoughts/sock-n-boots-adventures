SOUNDS = [
    [
        [],
        [
            {l:0.0, r:0.15, t:0.25, b:0.5, name:'door_knock.mp3'},
            {l:0.5, r:1, t:0.5, b:0.75, name:'sock_and_boots_laugh_tohether_as_they_eat.mp3'}
        ],
        [
            {l:0.05, r:0.23, t:0.2, b:0.6, name:'sock_laughing_as_he_runs.mp3'},
            {l:0.6, r:1, t:0.25, b:0.5, name:'individual_birds.mp3'}
            //{l:0.8, r:1, t:0.3, b:0.8, name:'boots_laughing_by_the_tree.mp3'}
        ],
         // Page 3
        [
            {l:0, r:0.6, t:0, b:0.75, name:'group_of_birds.mp3'},
            {l:0.61, r:1, t:0.05, b:0.3, name:'individual_birds.mp3'},
            {l:0.71, r:0.95, t:0.31, b:0.6, name:'apple_bite.mp3'}

        ],
         // Page 4
        [
            {l:0, r:0.4, t:0.4, b:0.8, name:'sock_laughing_as_he_runs.mp3'},
            {l:0.6, r:1, t:0.05, b:0.9, name:'boots_saying_mmm.wav'}
        ],
         // Page 5
        [
            {l:0.8, r:1, t:0.2, b:0.6, name:'crying_sock.mp3'},
            {l:0.3, r:1, t:0.6, b:1, name:'marching_ants.mp3'},
            {l:0, r:0.1, t:0.1, b:0.5, name:'lady_bug_flight.mp3'}
            
        ],
         // Page 6
        [
            {l:0.1, r:0.7, t:0.1, b:0.7, name:'sock_and_boots_laugh_tohether_as_they_eat.mp3'}
        ]

    ],
    [ [],
        // Page 1
        [
            {l:0.05, r:0.2, t:0.05, b:0.9, name:'brids_snore.mp3'},
            {l:0.21, r:0.4, t:0.4, b:0.75, name:'car_honk.mp3'},
            {l:0.35, r:0.65, t:0.35, b:0.75, name:'dog.mp3'}
        ],
         // Page 2
        [
            {l:0, r:0.2, t:0.2, b:0.7, name:'clock.mp3'},
            {l:0.6, r:0.9, t:0.5, b:1, name:'sock_screaming.mp3'}
        ],
         // Page 3
        [
            {l:0.1, r:0.3, t:0.25, b:0.6, name:'sock_looking_afraid_in_bed_2.mp3'}
        ],
         // Page 4
        [
            {l:0.4, r:0.7, t:0.5, b:1, name:'sock_looking_afraid_in_bed.mp3'}
        ],
        // Page 5
        [
            {l:0.4, r:0.7, t:0.25, b:0.75, name:'closet_door.mp3'}
        ],
        [],
        // Page 6
        [
            {l:0.7, r:0.9, t:0.5, b:0.9, name:'sock_checking_closest.mp3'}
        ],
        // Page 7
        [
            {l:0, r:0.5, t:0.5, b:1, name:'cat.mp3'}
        ]
    ],
    [
        [],
        // Page 1
        [
            {l:0.0, r:0.15, t:0.6, b:0.95, name:'sock_whistling.mp3'}
        ],
        // Page 2
        [
            {l:0.8, r:1, t:0, b:1, name:'washing_machine.wav'}

        ],
        // Page 3
        [
            {l:0, r:0.3, t:0.5, b:1, name:'boots_thinking.mp3'},
            {l:0.7, r:1, t:0.2, b:0.7, name:'sock_and_boots dizzy.mp3'}
        ],
        // Page 4
        [
            {l:0, r:0.4, t:0.2, b:1, name:'washing_machine.wav'},
            {l:0.5, r:1, t:0.15, b:0.7, name:'water_faucet.mp3'}
        ],
        [],
         // Page 6
        [
            {l:0, r:0.3, t:0.15, b:0.6, name:'washing_machine.wav'},
            {l:0.1, r:0.4, t:0.61, b:0.9, name:'boots_struggling.wav'},
            {l:0.5, r:0.8, t:0.15, b:0.7, name:'water_faucet.mp3'}
        ],
        [
             {l:0, r:1, t:0, b:1, name:'sock_splashes.wav'}
        ],
        [
             {l:0.4, r:0.8, t:0.6, b:0.9, name:'sock_feeling_sick.mp3'}
        ],
        [

        ]

    ]
];