HEADER_TITLE = "Libro di storia";
TITLES = ["Condividere", "Paura del buio", "Giocare in sicurezza"];
PAGE_NUMBERS = [
    [1,2,3,4,5,6,7,8,9],
    [1,2,3,4,5,6,8,9,10,11,12],
    [1,2,3,4,5,6,7,8,9,10,11]
];

BUTTONS = {"help" : "Aiuto", "main_menu" : "Menu Principale", "story_book" : "Libro",
    "read_myself" : "Leggi Tutto", "read_to_me" : "Leggermi", "story_menu" : "Storia di menù"};

TEXTS = [
    [
        [
            {text:"Sai perché si dovrebbe condividere?", time:2.6},

            {text:"Calzino e il suo migliore amico Stivali", time:5},
            {text:"te lo vogliono spiegare.", time:7.2}
        ],
        [
            {text:"Un giorno Calzino e Stivali", time:9.5},
            {text:"decidono di uscire fuori a giocare.", time:12},
            {text:"Preparano un pranzo al sacco", time:14.2},
            {text:"e proseguono per la loro strada.", time:16},
            {text:'Calzino dice, "Andiamo a giocare nel parco."', time:18.8},
            {text:'Stivali risponde,', time:21},
            {text:'"Cerchiamo di tornare prima che faccia buio."', time:23.5}
        ],

        [
            {text:"Una volta arrivati ​​al parco,", time:25.4},
            {text:"lasciano il pranzo al sacco sul tavolo.", time:28},
            {text:'Calzino dice. “E’ora del divertimento."', time:32},
            {text:"Giocano a nascondino, ridono e corrono.", time:36.2}
        ],

        [
            {text:"Arrivano degli uccelli", time:38},
            {text:"e mangiano il pranzo di Calzino.", time:40.2},
            {text:"Mangiano e mangiano", time: 41.5},
            {text:"e sgranocchiano e sgranocchiano.", time: 43.5},
            {text:"Calzino e Stivali torneranno più tardi.", time:47.2},
            {text:"Ma cosa farà Calzino? Il suo pranzo non c’è più.", time:52.3}
        ],
        [
            {text:'"Ho fame!", dice Stivali, "andiamo a mangiare."', time:56.7},
            {text:"Stivali sa che nel suo pranzo", time:58.4},
            {text:"c’è una sorpresa speciale.", time:61.3},
            {text:"Hanno giocato per ore, ed ora è mezzogiorno.", time:64.5},
            {text:"“Torniamo al tavolo,” dice Stivali a Calzino.", time:69}
        ],
        [

            {text:'"Oh no!", dice Calzino con grande sorpresa,', time:60+13.8},
            {text:'"Il mio pranzo non c’è più", e così inzia a piangere.', time:60+19.4},
            {text:'"Non ti preoccupare", dice Stivali,', time:60+22.3},
            {text:'"io ho tanto da mangiare,', time:60+25},
            {text:'possiamo entrambi condividere il mio pranzo."', time:60+28}

        ],
        [
            {text:"Calzino ora è felice che abbia qualcosa da mangiare.", time:60+32},
            {text:"Stivali condivide anche la sua torta al cioccolato,", time:60+36.2},
            {text:"la sua preferita.", time:60+37.4},
            {text:"Ora anche Stivali è felicissimo,", time:60+39.4},
            {text:"perchè condividere ha reso Calzino felice,", time:60+43},
            {text:"e condividere è una cosa buona da fare.", time:60+46.5}
        ],
        [
            {text:"Ora vedi perché si dovrebbe condividere?", time:60+50}

        ],
        []


    ],
    /* Story 2*/
    [
        [
            {text:"Hai mai paura del buio durante la notte?", time:3},
            {text:"Calzino vuole raccontarti", time:5},
            {text:"come ha superato la sua paura del buio.", time:8.5}
        ],
        [
            {text:"Calzino sa che alle otto di sera è già buio.", time:12},
            {text:"Gli uccelli non cantano più", time:13.6},
            {text:"ed i cani smettono di abbaiare.", time:16},
            {text:"L'intero quartiere spegne le luci,", time:18.2},
            {text:"e tutti vanno a dormire per la notte.", time:24}
        ],

        [
            {text:"Tic... tac.... tic.... tac.", time:28.5},

            {text:"L'orologio segna le otto", time:30.5},
            {text:"ed è ora che Calzino vada a letto.", time:33.5},
            {text:"Si preparerà per andare a letto,", time:36},
            {text:"e poi spegnerà la luce,", time:37.6},
            {text:"ma Calzino di notte ha paura del buio.", time:40.7}
        ],

        [
            {text:"Tira su le coperte fino alla testa,", time:44},
            {text:"e si trova lì a tremare nel suo letto.", time:47.6},
            {text:'"Ho paura, ho paura", Calzino inizia ad urlare.', time:52.7},
            {text:'"Non mi piace quando si spengono le luci."', time:56.2}
        ],

        [
            {text:"A Calzino non piace spegnere la luce,", time:58.5},

            {text:"perché durante la notte", time:60.2},
            {text:"sente in casa dei rumori spaventosi.", time:63},
            {text:"Ora la cameretta di Calzino è completamente al buio,", time:67.2},
            {text:"solo lo splendore della luna fa penetrare un pò di luce.", time:74}
        ],

        [
            {text:"Scricc... scricc.... fa il pavimento.", time:60+19},
            {text:"ma è un rumore", time:60+20.5},
            {text:"che viene da dietro la porta dell'armadio?", time:60+23},
            {text:"Il solo pensiero", time:60+25},
            {text:"dà a Calzino uno spavento davvero grande.", time:60+28.6},
            {text:"Potrebbe esserci qualcosa che si nasconde lì dentro?", time:60+36}
        ],

        [
            {text:"Calzino è molto curioso,", time:60+40},

            {text:"così salta fuori dal letto per andare a vedere.", time:60+45.4},
            {text:"Accende la luce ed apre l'armadio,", time:60+50},
            {text:"e non c'è nessun mostro da nessuna parte.", time:60+55}
        ],

        [
            {text:"Tanto per assicurasi, Calzino guarda dappertutto, ", time:60+58.7},
            {text:"poi salta di nuovo nel suo letto", time:121.2},
            {text:"per dormire tranquillo.", time:123.6},
            {text:'"Sono contento di aver controllato", dice Calzino.', time:128.3},
            {text:'"Ora non avrò più paura"', time:120+9.5},
            {text:'"quando vado a dormire la sera."', time:120+13.6}
        ],

        [
            {text:"Così, quando Calzino ha paura del buio di notte", time:120+19},
            {text:"controlla tutta la cameretta con la luce accesa.", time:120+22.4},
            {text:"Ora Calzino può dormire senza preoccupazioni,", time:120+26},
            {text:"perché non c’è nessun mostro.", time:120+33}
        ],
        [
            {text:"Ora vedi come si può superare", time:120+35},
            {text:"la paura del buio?", time:120+37}
        ],
        [
        ]
    ],

    /* Story 3*/
    [
        [
            {text:"Dovresti fare attenzione dove giochi?", time:3.2},

            {text:"Calzino e Stivali", time:4.8},
            {text:"vogliono spiegarti perchè dovresti fare attenzione.", time:8}
        ],

        [
            {text:"Ci sono così tanti luoghi dove giocare in casa;", time:11.5},
            {text:"anche sotto il letto come un piccolo topo.", time:15.5},
            {text:"Calzino vuole trovare un nuovo posto dove giocare.", time:20.3},
            {text:"Ma dove giocherà oggi?", time:23.5}
        ],

        [
            {text:"Calzino vuole giocare in lavatrice.", time:27.2},
            {text:"La lavatrice gira e gira", time:29},
            {text:"per poter lavare gli indumenti.", time:32},
            {text:"Calzino dice a Stivali,", time:34.7},
            {text:'"entriamo lì dentro, sembra divertente.... ', time:37.7},
            {text:'"possiamo fare un giro."', time:39.8}
        ],

        [
            {text:'"No!", dice Stivali, "non possiamo entrarci."', time:43},
            {text:'"Potremmo farci male, lì dentro!"', time:46.5},
            {text:'"Penso che dovremmo cercare un altro posto."', time:50},
            {text:'"Dovremmo giocare solo dove sappiamo"', time:52.8},
            {text:'"di essere al sicuro."', time:55}
        ],

        [
            {text:'Calzino dice, "Io voglio entrare e mi divertirò,', time:59},
            {text:'e tu puoi stare a guardare"', time:60.7},
            {text:'fino a quando non avrò finito."', time:62.5},
            {text:"Stivali comincia a preoccuparsi per il suo amico.", time:66},
            {text:'"Non farlo", dice Stivali, ', time:68.6},
            {text:'ma Calzino entra lo stesso nella lavatrice.', time:60+12}
        ],

        [
            {text:"La lavatrice è in funzione, gira e rigira", time:60+15.5},
            {text:"Si riempie di acqua mentre Calzino rotola e rirotola.", time:60+20},
            {text:'"Aiutami Stivali!", inizia ad urlare Calzino ,', time:60+23},
            {text:'"per favore fammi uscire da questa lavatrice!"', time:60+27}
        ],


        [
            {text:"Stivali si mette sopra e cerca di aprire la porta,", time:60+31},
            {text:"ci prova e ci riprova poi tenta ancora.", time:60+34},
            {text:"La porta non si apre perchè è bloccata ", time:60+38},
            {text:"come farà Stivali a far uscire il povero Calzino?", time:60+43}
        ],

        [
            {text:"Calzino urla di nuovo,", time:60+45.4},
            {text:'"Per favore togli la spina della lavatrice!"', time:60+50},
            {text:'"Giusto!" dice Stivali. Ma dov\'è la spina?', time:60+54.5},
            {text:"Guarda dappertutto, anche sotto il tappeto.", time:60+58.5}
        ],
        [
            {text:'"Eccola!" Stivali comincia a gridare.', time:121.6},
            {text:"Corre verso il muro e toglie la spina.", time:125},
            {text:"La lavatrice si ferma e la porta si sblocca. ", time:129.5},
            {text:"Stivali ha salvato il suo migliore amico Calzino.", time:120+14}
        ],
        [

            {text:"Ora Calzino è al sicuro,", time:120+16.5},
            {text:"e oggi ha imparato, che dovrebbe giocare", time:120+20},
            {text:"solo dove sa di essere al sicuro.", time:120+22},
            {text:"Riesci a pensare a luoghi", time:120+25},
            {text:"che non sono sicuri per giocare?", time:120+27.5}

        ],
        [
        ]
    ]
];