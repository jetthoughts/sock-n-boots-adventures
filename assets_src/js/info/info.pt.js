HEADER_TITLE = "Meia e Bota";
TITLES = ["Compartilhar", "Medo do escuro. ","Brinque com segurança."];
PAGE_NUMBERS = [
    [1,2,3,4,5,6,7,8,9],
    [1,2,3,4,5,6,8,9,10,11,12],
    [1,2,3,4,5,6,7,8,9,10,11]
];

BUTTONS = {"help" : "Ajudar", "main_menu" : "Menú Principal", "story_book" : "Libro",
    "read_myself" : "Leia-me", "read_to_me" : "Leia mim", "story_menu" : "Menú História"};

TEXTS = [
    [
        [
            {text:"Você sabe por que você deve compartilhar?", time:3},
            {text:"Meia e seu melhor amigo Bota vão mostra-lhe.", time:7}
        ],
        [
            {text:"Um dia Meia e Bota saem para brincar.", time:11.4},
            {text:"Preparam seus lanches e vão passear.", time:6 + 9.4},
            {text:"Meia diz, “Vamos brincar no parque.”", time:6 + 13.8},
            {text:"Bota diz, “Vamos voltar antes que seja tarde.”", time:25.2}
        ],

        [
            {text:"Assim que eles chegam no parque, ", time:28.4},
            {text:"arrumam o almoço  na mesa. ", time:32},
            {text:"Meia diz, “Agora é hora de alegria.”", time:36.3},
            {text:"Eles brincam de esconde-esconde e correm na folia.", time:42.2}
        ],

        [
            {text:"Os pássaros que voam comem o lanche do Meia.", time:48},
            {text:"E comem e comem e mmmm e mmmm.", time:55},
            {text:"Meia e Bota voltam mais tarde. ", time:59},
            {text:"Mas o que houve com o almoço do Meia?", time:63},
            {text:"Os pássaros devoraram tudo.", time:67}

        ],
        [
            {text:"“Tenho fome”, diz Bota, “vamos comer” ", time:72},
            {text:"Bota sabe que seu almoço é delicioso. ", time:77},
            {text:"Eles brincam muito e é meio-dia. ", time:81.2},
            {text:"“Voltamos para à mesa,” diz  Bota a Meia. ", time:86.4}
        ],
        [

            {text:"Oh, não! Diz Meia com grande surpresa, ", time:92},
            {text:"Meu almoço sumiu, e começa a chorar.", time:97},
            {text:"Não se preocupe, diz Bota, tenho para os dois", time:103.2},
            {text:"Podemos dividir meu almoço.", time:107}

        ],
        [

            {text:"Meia está feliz porque também tem um almoço.", time:111},
            {text:"Bota até divide o bolo de chocolate", time:115.4},
            {text:"seu doce favorito.", time:118},
            {text:"Bota está feliz também.", time:121.2},
            {text:"Porque compartilhar faz Meia feliz,", time:125.2},
            {text:"e compartilhar é bom demais. ", time:128.6}
        ],
        [
            {text:"Agora você sabe por que você deve compartilhar? ", time:134.8}

        ],
        []


    ],
    /* Story 2*/
    [
        [
            {text:"Você tem medo do escuro à noite ? ", time:2.5},
            
            {text:"Meia vai dizer como", time:4.5},
            {text:"você pode vencer seu medo do escuro.", time:7.4}
        ],
        [
            {text:"Às 8 horas na casa do Meia escurece.", time:12.8},
            {text:"Os pássaros não cantam e os cães não latem.", time:19},
            {text:"Todos os vizinhos apagam as luzes também,", time:23},
            {text:"e todos vão dormir.", time:26.5}
        ],

        [
            {text:"Tick...tock....tick....tock.", time:30},

            {text:"O relógio bate 8 horas e hora de dormir, Meia.", time:35},
            {text:"Ele se arruma e apaga a luz,", time:38},
            {text:"mas Meia está com medo do escuro. ", time:42}
        ],

        [
            {text:"Ele cobre a cabeça com a coberta,", time:46},
            {text:"e fica tremendo quietinho.", time:50},
            
            {text:"“Estou com medo, estou com medo”", time:52.8},
            {text:"Meia começa a gritar.“", time:55.4},
            {text:"“Não gosto de ficar no escuro.” ", time:59}
        ],

        [
            {text:"Meia não gosta de apagar a luz, ", time:63},

            {text:"porque a casa tem ruídos assustadores à noite, ", time:67.2},
            {text:"Agora está escuro no quarto do Meia, ", time:71},
            {text:"há apenas a luz da lua que brilha.", time:74}
        ],

        [
            {text:"Cric… Cric… faz o pavimento.", time:80},
            {text:"É um ruído que vem da porta do armário?", time:84},
            {text:"O pensamento realmente assusta Meia.", time:88},
            {text:"Será que tem algo escondido ali?", time:92}
        ],

        [
            {text:"Meia está muito curioso", time:96},
            {text:"então ele pula da cama para ver. ", time:100},
            {text:"Ele abre o armário e acende a luz, ", time:104.4},
            {text:"e não há nenhum monstro.", time:107.5}
        ],

        [
            {text:"Para ter certeza, Meia procura em todos os lugares", time:113.4},
            {text:"e volta para cama para dormir tranquilo.", time:117},
            {text:"Estou feliz por ter procurado, diz Meia.", time:121},
            {text:"Agora não vou ter medo às 8 horas da noite.", time:124.4}
        ],

        [
            {text:"Assim quando Meia tiver medo à noite ", time:129},
            {text:"Ele procura em todos os lugares quando acende a luz", time:133},
            {text:"Agora Meia pode dormir sem medo ", time:137},
            {text:"porque não há monstros", time:142}

        ],
        [
            {text:"Agora você sabe como superar o medo do escuro?", time:148.5}
        ],
        [
        ]
    ],

    /* Story 3*/
    [
        [
            {text:"Você deve ter cuidado com o lugar onde brinca?", time:3.5},
            {text:"Meia e Bota querem mostrar-lhe", time:6.5},
            {text:"por que você deve ser cuidadoso.", time:9.5}
        ],

        [
            {text:"Há muitos  lugares para brincar em casa.", time:13.4},

            {text:"Até mesmo embaixo da cama como um camundongo.", time:18},
            {text:"Meia quer descobrir um novo lugar para brincar.", time:22},
            {text:"Então onde ele brincará hoje?", time:25.2}
        ],

        [
            {text:"Meia quer brincar na lavadora", time:28.8},
            {text:"que bate e bate para limpar a roupa. ", time:32},

            {text:"Meia diz a Bota, ", time:35},
            {text:"“Vamos entrar, me parece divertido...", time:38},

            {text:"vamos dar uma volta de brincadeira.", time:41.5}
        ],

        [
            {text:"“Não,” diz Bota, “não podemos ali entrar”", time:47},
            {text:"“Podemos nos machucar.”", time:49.2},
            {text:"“Acho que deveríamos procurar um lugar diferente.”", time:53.4},
            {text:"“Devemos brincar apenas onde sabemos", time:57},
            {text:"que é seguro para gente.”", time:60}
        ],

        [
            {text:"Meia diz, “Pois vou entrar e você vai ficar", time:65},
            {text:" aí olhando até eu me acabar de brincar.”", time:69.2},
            {text:"Bota está preocupado com o amigo.", time:72.2},
            {text:"“Não faça isso,” diz Bota, ", time:75.2},
            {text:"mas Meia continua a subir.", time:78.4}
        ],

        [
            {text:"A lavadora sacudia e sacudia, travando a porta.", time:85},
            {text:"Encheu-se de água e Meia dava cambalhota.", time:90},
            {text:"“Socorro, Botas,”  Meia começou a gritar,", time:95},
            {text:"“por favor, me tire daqui desse lugar.”", time:100}
        ],


        [
            {text:"Bota sobe, chega ao  topo e agarra-sea porta,", time:106.3},
            {text:"puxa, puxa. Mas a porta não abre.", time:110.4},
            {text:"Como Bota soltaria-o?", time:114}
        ],

        [
            {text:"Meia solta outro grito apavorado,", time:118.4},
            {text:"por favor tire o plugue da tomada!", time:122},
            {text:"“Boa ideia!“ Diz Bota, mas onde está a tomada?", time:127.4},
            {text:"Procurando até debaixo do tapete sem achar nada.", time:132}
        ],
        [
            {text:"Já vou! Bota grita correndo para a parede", time:137},
            {text:"apressado para puxar a tomada.", time:140},
            {text:"A lavadora para, a porta destrava", time:144},
            {text:"e Bota o amiga Meia salva.", time:148}
        ],
        [

            {text:"Agora Meia está salvo e por hoje aprendeu sua lição.", time:153.8},
            {text:"Não deve brincar onde não seja seguro.", time:158},
            {text:"Você consegue pensar em lugares", time:160.5},
            {text:"que são seguros para você brincar?", time:163.5}

        ],
        [
        ]
    ]
];