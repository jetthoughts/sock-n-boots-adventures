HEADER_TITLE = "Bibliothèque";
BUTTONS = {"help" : "Aider", "main_menu" : "Menu principal", "story_book" : "Bibliothèque",
    "read_myself" : "lu moi-même", "read_to_me" : "Lisez-moi", "story_menu" : "Menu histoire"};
TITLES = ["Partager", "Peur de l’obscurité", "Joue en toute sécurité"];

PAGE_NUMBERS = [
		[1,2,3,4,5,6,7,8,9],
		[1,2,3,4,5,6,8,9,10,11,12],
		[1,2,3,4,5,6,7,8,9,10,11]
];

TEXTS = [
    [
        [
            {text:"Sais-tu pourquoi tu devrais partager?", time:3},
            {text:"Chaussette et son meilleur ami Bottines vont te l’expliquer.", time:7}
        ],
        [
            {text:"Un jour Chaussette et Bottines décident de sortir jouer.", time:12},
            {text:"Ils préparent leurs déjeuners et se mettent en route.", time:15},
            {text:"Chaussette dit, «Allons donc jouer au parc».", time:19},
            {text:"Bottines s’exclame, «Rentrons avant que la nuit ne tombe!»", time:24}
        ],

        [
            {text:"Arrivés au parc,", time:25},
            {text:"Ils installent leur déjeuner sur la table.", time:27},
            {text:"Chaussette s’écrie «Il est temps de s’amuser maintenant!»", time:32},
            {text:"Ils se mettent à jouer à chat, ils rient, ils courent.", time:36}
        ],

        [
            {text:"Soudain des oiseaux s’approchent et mangent", time:38.5},
	          {text:"le déjeuner de Chaussette.", time:40.5},
            {text:"Ils mangent et mangent, dévorent et dévorent encore.", time:44},
            {text:"Chaussette et Bottines reviendront dans quelques temps.", time:47},
            {text:"Mais que fera Chaussette? Son repas n’est plus là.", time:52}

        ],
        [
            {text:"«J’ai faim», dit Chaussette, «Allons manger!»", time:56},
            {text:"Bottines sait bien que son déjeuner", time:58},
	          {text:"sera particulièrement savoureux.", time:61},
            {text:"Tous deux ont joué pendant des heures et il est déjà midi.", time:60 + 4},
            {text:"«Rejoignons donc notre table» dit Bottines à Chaussette.", time:60 + 8}
        ],
        [

            {text:"«Oh non!» s’écrie Chaussette dans une grande surprise,", time:60 + 13},
            {text:"«Mon repas n’est plus», et il se met à pleurer.", time:60 + 19},
            {text:"«Ne t’inquiète pas» dit Bottines, «j’ai beaucoup à déjeuner,", time:60 + 25},
            {text:"Nous pouvons bien partager mon repas».", time:60 + 28}

        ],
        [

            {text:"Chaussette est heureux, puisqu’il a de nouveau à déjeuner.", time:60 + 32},
            {text:"Bottines partage même son gâteau au chocolat", time:60 + 35},
	          {text:"son mets favori.", time:60 + 37},
            {text:"Bottines se sent très heureux aussi maintenant.", time:60 + 40},
            {text:"Parce que Chaussette a retrouvé le sourire,", time:60 + 42},
	          {text:"et que partager fait du bien.", time:60 + 44}
        ],
        [
            {text:"Maintenant, vois-tu mieux pourquoi il est bon de partager?", time:60 + 49}

        ],
        []


    ],
    /* Story 2*/
    [
        [
            {text:"As-tu peur de l’obscurité la nuit?", time:4},
            {text:"Chaussette veut t’expliquer comment", time:6},
	          {text:"il s’est débarrassé de ses craintes dans le noir.", time:8}
        ],
        [
            {text:"A 8 heures, la maison de Chaussette devient sombre.", time:13},
            {text:"Les oiseaux ne chantent plus et les chiens n’aboient plus,", time:19},
            {text:"Tout le quartier éteint ses lumières,", time:21},
            {text:"Et tous ses habitants rejoignent leurs lits.", time:27}
        ],

        [
            {text:"Tic..Tac..Tic..Tac.", time:31},
            {text:"L’horloge frappe les 8 heures", time:33},
	          {text:"et de se coucher pour Chaussette il est l’heure.", time:36},
            {text:"Il se prépare pour se coucher, et puis il éteindra les lumières.", time:41},
            {text:"Mais Chaussette a peur dans le noir le soir.", time:44}
        ],

        [
            {text:"Il remonte ses couvertures jusqu’à sa tête,", time:47},
            {text:"Et reste là couché, tremblant dans son lit.", time:51},
            {text:"«J’ai peur», «J’ai peur», commence à crier Chaussette,", time:56},
            {text:"«Je n’aime pas quand les lumières s’en vont!»", time:59}
        ],

        [
            {text:"Chaussette n’aime pas éteindre les lumières,", time:60 + 3},
            {text:"Parce que la maison fait de drôles de bruits la nuit.", time:60 + 7},
            {text:"Tout est noir maintenant dans la chambre de Chaussette,", time:60 + 11},
            {text:"Même s’il reste les lumières de la Lune.", time:60 + 15}
        ],

        [
            {text:"Crrr...Crrr… fait le parquet,", time:60 + 21},
            {text:"Ou est-ce un bruit que fait la porte du placard?", time:60 + 25},
            {text:"Cette idée fait très peur à Chaussette.", time:60 + 27},
            {text:"Y aurait-il quelque chose, caché dans un tiroir?", time:60 + 30}
        ],

        [
            {text:"La curiosité de Chaussette ne pourrait pas être plus attisée.", time:60 + 35},
            {text:"Il saute soudain du lit pour aller voir qui fait ce bruit.", time:60 + 38},
            {text:"Il ouvre le placard et allume la lumière,", time:60 + 41},
            {text:"Mais dans le placard il n’y a pas de monstres, ni sorcières.", time:60 + 45}
        ],

        [
            {text:"Pour se rassurer encore, Chaussette inspecte tout autour de lui,", time:60 + 50},
            {text:"Et puis il saute à nouveau dans son lit,", time:60 + 53},
            {text:"Pour dormir en sûreté et en toute tranquillité.", time:60 + 56},
            {text:"«Je suis heureux d’avoir vérifié moi-même», dit Chaussette,", time:121},
	          {text:"«Maintenant je n’aurai plus peur à 8 heures.»", time:120 + 3}
        ],

        [
            {text:"Désormais lorsque Chaussette a peur la nuit,", time:120 + 7},
            {text:"Il inspecte toute sa chambre éclairée,", time:120 + 10},
            {text:"Et c’est ainsi qu’il peut dormir sur ses deux oreilles,", time:120 + 15},
            {text:"Maintenant qu’il sait qu’aucun monstre ne le surveille.", time:120 + 23}
        ],
        [
            {text:"Et toi maintenant?", time:120 + 26},
	          {text:"Vois-tu comment te débarrasser de tes peurs du noir?", time:120 + 33}
        ],
        [
        ]
    ],

    /* Story 3*/
    [
        [
            {text:"Dois-tu faire attention quand tu joues?", time:2},

            {text:"Chaussette et Bottines veulent te montrer", time:4.5},
            {text:"pourquoi il est bon de faire attention.", time:7}
        ],

        [
            {text:"Il y a tant d’endroits pour jouer dans une maison.", time:11},
            {text:"Y compris sous le lit, comme une toute petite souris.", time:15},
            {text:"Chaussette veut trouver un nouveau coin pour jouer.", time:18},
            {text:"Où jouera t-il donc aujourd’hui?", time:22}
        ],

        [
            {text:"Chaussette veut jouer dans la machine à laver.", time:25},
            {text:"Elle tourne et tourne, pour rendre le linge propre.", time:31},
            {text:"Un jour, Chaussette dit à Bottines, «Allons donc dedans,", time:34.5},
            {text:"ça semble amusant, allons-donc y faire un tour».", time:38}
        ],

        [
            {text:"«Non», répond Bottines, «Nous ne pouvons pas y faire un tour»,", time:43},
            {text:"«Nous pourrions nous faire mal»,", time:45},
            {text:"«Nous devrions seulement jouer", time:46},
            {text:"là où nous savons que nous sommes en sécurité».", time:53}
        ],

        [
            {text:"Chaussette répond, «J’irai dedans, et je m’amuserai seul,", time:58},
            {text:"et toi tu n’auras qu’à regarder et attendre!»", time:60+1},
            {text:"Alors Bottines commence à s’inquiéter pour son ami et lui dit", time:60 + 5},
            {text:"«N’y va pas», mais Chaussette est déjà occupé à grimper.", time:60 + 10}
        ],

        [
            {text:"La machine à laver se referme alors,", time:60 + 12.5},
	          {text:"et commence à tourner, et gronder.", time:60 + 14},
            {text:"Puis elle se remplit d’eau pendant", time:60 + 16},
	          {text:"que Chaussette culbute et culbute.", time:60 + 19},
            {text:"«Aide-moi Bottines», commence à crier Chaussette,", time:60 + 23},
            {text:"«Je t’en prie, sors-moi de cette machine».", time:60 + 26}
        ],


        [
            {text:"Bottines grimpe très haut,", time:60 + 28},
            {text:"tire sur la porte, et essaye et essaye encore.", time:60 + 32},
            {text:"La porte ne s’ouvrira pas, car elle est verrouillée.", time:60 + 37},
            {text:"Comment Bottines pourra t-il libérer le pauvre Chaussette?", time:60 + 41}
        ],

        [
            {text:"Chaussette se met alors à crier,", time:60 + 43},
            {text:"«Alors débranche la prise de la machine à laver s’il te plait!»", time:60 + 48},
            {text:"«Voilà!», dit Bottines,", time:60 + 50},
	          {text:"«Mais où est donc la prise de cette machine à laver?»", time:60 + 54},
            {text:"Il regarde partout, même sous le tapis.", time:60 + 57}
        ],
        [
            {text:"«La voilà!», s’écrie Bottines", time:120},
            {text:"et il court vers le mur, et débranche vite la prise.", time:120 + 4},
            {text:"Alors la machine à laver s’arrête, et se déverrouille.", time:120 + 8},
            {text:"Bottines vient de sauver son meilleur ami Chaussette.", time:120 + 12.5}
        ],
        [

            {text:"Chaussette est maintenant en sécurité,", time:120 + 15.5},
	          {text:"et a appris quelque chose aujourd’hui,", time:120 + 18},
            {text:"qu’il ne devrait jouer que dans des endroits sûrs pour lui.", time:120 + 22},
            {text:"Et toi, saurais-tu citer des endroits où", time:120 + 25},
	          {text:"il ne serait pas sûr pour toi de jouer?", time:120 + 27}

        ],
        [
        ]
    ]
]