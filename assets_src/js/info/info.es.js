HEADER_TITLE = "Livro da história";
TITLES = ["Compartir", "Miedo a la oscuridad", "Jugar con seguridad"];
PAGE_NUMBERS = [
    [1,2,3,4,5,6,7,8,9],
    [1,2,3,4,5,6,8,9,10,11,12],
    [1,2,3,4,5,6,7,8,9,10,11]
];

BUTTONS = {"help" : "Ayude", "main_menu" : "Menú Principal", "story_book" : "Livro",
    "read_myself" : "Lea mi auto", "read_to_me" : "Leer para mí", "story_menu" : "Menú Historia"};

TEXTS = [
    [
        [
            {text:"Sabes por qué deberías compartir?", time:3},

            {text:"Medias y su mejor amigo desean mostrartelo", time:7}
        ],
        [
            {text:"Un día Medias y Botas van a jugar.", time:11.4},
            {text:"Envuelven su almuerzo y se van", time:6 + 9.4},
            {text:"Medias dice, “vamos a jugar en el parque.”", time:6 + 13.2},
            {text:"Botas dice, “regresemos antes que oscurezca.”", time:23.2}
        ],

        [
            {text:"Una vez que llegan al parque.", time:23 + 2.7},
            {text:"Dejan su almuerzo sobre la mesa", time:23 + 5.2},
            {text:"Medias dice, “ahora es tiempo de divertirnos.”", time:32},
            {text:"Juegan a las escondidas y ríen y corren", time:36}
        ],

        [
            {text:"Los pájaros llegan volando y se comen", time:36 + 2.8},
            {text:"el almuerzo de Medias.", time: 40.4},
            {text:"Comian y comian y masticaban y masticaban.", time:36 + 7.4},
            {text:"Medias y Botas regresaran mas tarde.", time:36 + 11},
            {text:"Pero qué hará Medias? Su almuerzo desapareció.", time:52.4}

        ],
        [
            {text:"Tengo hambre”, dice Botas, “vamos a comer.”", time:56.5},
            {text:"Botas sabe que su almuerzo", time:58.7},
            {text:"tiene una golosina especial.", time:61},
            {text:"Ellos han jugado durante horas, y son las  12 del día.", time:52 + 13},
            {text:"“Regresemos  la mesa,” le die Botas a Medias.", time:69.4}
        ],
        [

            {text:"Oh no! Dice Medias con gran sorpresa.", time:74.4},
            {text:"Mi almuerzo desapareció, mientras empieza a llorar.", time:80},
            {text:"No te preocupes, dice Botas. Yo tengo bastante,", time:84},
            {text:"Nosotros podemos compartir mi almuerzo.", time:88}

        ],
        [

            {text:"Medias está contento", time:89.8},
            {text:"ahora que hay almuerzo para comer.", time:91.3},
            {text:"Botas  comparte inclusive su torta de Chocolate,", time:94.8},
            {text:"su golosina favorita.", time:97},
            {text:"Ahora Botas también está contento.", time:100.5},
            {text:"Porque el compartir hizo feliz a Medias", time:103},
            {text:"y compartir es bueno.", time:106}
        ],
        [
            {text:"Ahora ves porque deberías compartir?", time:109}

        ],
        []


    ],
    /* Story 2*/
    [
        [
            {text:"Tienes miedo a la obscuridad en la noche?", time:3.5},
            {text:"Medias quiere decirte cómo logró", time:6},
            {text:"superar su miedo ala obscuridad.", time:8.5}
        ],
        [
            {text:"A las 8 de la noche la casa de Medias se pone oscura", time:8 + 6.5},
            {text:"Los pájaros no cantan, y los perros no ladran", time:8 + 12.5},
            {text:"Todo el vecindario apaga sus luces,", time:8 + 15.5},
            {text:"Y todos van a dormir por la noche.", time:26.5}
        ],

        [
            {text:"Tic….toc….tic…toc", time:26 + 6},

            {text:"El reloj marca las 8 y es hora de dormir para Medias", time:26 + 10.2},
            {text:"El se alista para la cama, luego el apaga la luz,", time:42},
            {text:"Pero Medias le tiene miedo", time:43.7},
            {text:"a la oscuridad por la noche.", time:46}
        ],

        [
            {text:"El tira de sus colchas hasta cubrir su cabeza,", time:49},
            {text:"Y se queda temblando en su cama", time:53},
            {text:"“Tengo miedo,  tengo miedo”,", time:55},
            {text:"“Medias comienza a gritar", time:57},
            {text:"“No me gusta cuando se apagan las luces.”", time:62}
        ],

        [
            {text:"A  Medias no le gusta apagar las luces", time:65},

            {text:"Porque la casa hace un ruido de miedo en la noche.", time:68.5},
            {text:"Ahora está oscuro dentro de la habitación de Medias,", time:72},
            {text:"Excepto por la brillante luz de la luna.", time:76}
        ],

        [
            {text:"Cric… Cric… suena el piso", time:82.5},
            {text:"Está viniendo ese ruido de la puerta del armario?", time:85.5},
            {text:"El solo pensamiento le da a Medias", time:86.5},
            {text:"un  miedo realmente grande", time:89},
            {text:"Podría estar algo escondido allí?", time:92}
        ],

        [
            {text:"Medias es tan curioso cómo puede ser,", time:95.2},

            {text:"Así que salta de la cama para ir a mirar", time:98},
            {text:"El abre el armario y enciende la luz", time:102},
            {text:"Y no hay ningún monstruo a la vista.", time:106.2}
        ],

        [
            {text:"Solo para estar seguro Medias mira por todas partes,", time:112},
            {text:"luego vuelve a saltar a su cama", time:114},
            {text:"para dormir sano y salvo.", time:117},
            {text:"Me alegro de haber comprobado por mí mismo”", time:119.5},
            {text:"dice Medias.", time:120.5},
            {text:"“Ahora no tendré miedo a las 8 de la noche.”", time:123.5}
        ],

        [
            {text:"Así que cuando Medias tiene miedo", time:125},
            {text:"a la oscuridad en la noche,", time:126.7},
            {text:"Solamente revisa toda su habitación", time:129.5},
            {text:"cuando enciende la luz.", time:131.2},

            {text:"Ahora Medias puede dormir sin cuidado", time:133.8},
            {text:"Porque no hay monstruos allí", time:139}
        ],
        [
            {text:"Ahora ves cómo puedes superar", time:142.6},
            {text:"tu miedo a la obscuridad?", time:144.3}
        ],
        [
        ]
    ],

    /* Story 3*/
    [
        [
            {text:"Deberías tener cuidado donde juegas?", time:2.5},

            {text:"Medias y botas desean mostrarte porque", time:4.5},
            {text:"deberías ser cuidadoso", time:7}
        ],

        [
            {text:"Hay tantos lugares para jugar en la casa.", time:11},

            {text:"Inclusive debajo de la cama, como un pequeño ratón.", time:7 + 8},
            {text:"Medias quiere encontrar un nuevo lugar para jugar.", time:7 + 11.8},
            {text:"Dónde puede jugar sus juegos hoy?", time:23}
        ],

        [
            {text:"Medias quiere jugar en la lavadora de ropa.", time:26},

            {text:"Esta gira y gira para lavar las cosas.", time:23 + 8.3},
            {text:"Medias le dice a Botas, “vamos a entrar,", time:23 + 11.8},
            {text:"parece divertido…. Podemos dar un paseo.”", time:38.5}
        ],

        [
            {text:"“No! Dice Botas, “no podemos pasear.”", time:39 + 3.6},
            {text:"“Podríamos lastimarnos si entramos.”", time:46},
            {text:"“Creo que nosotros deberíamos buscar", time:48.5},
            {text:"un lugar diferente.”", time:50},
            {text:"“Nosotros solo deberíamos jugar", time:52.2},
            {text:"donde sabemos que es seguro.”", time:54.5}
        ],

        [
            {text:"Medias dice, “Yo entraré y me divertiré,", time:58.4},
            {text:"Y tú puedes mirar hasta que yo termine.", time:54 + 8},
            {text:"Botas empieza a preocuparse por su amigo,", time:65.5},
            {text:"“No lo hagas,” dice Botas,", time:68},
            {text:"“pero Medias sigue trepando.", time:71}
        ],

        [
            {text:"La lavadora se cierra, mientras suena y suena.", time:75},
            {text:"Esta se llena de agua mientras Medias  gira y gira.", time:79},
            {text:"“Ayúdame Botas,” Empieza a gritar Medias,", time:71 + 11.8},
            {text:"“por favor sácame de la lavadora.”", time:86.4}
        ],


        [
            {text:"Botas trepa a lo alto y tira de la puerta,", time:87 + 3.2},
            {text:"el trata,  y trata y trata más.", time:95},
            {text:"La puerta no se abrirá porque está cerrada", time:98},
            {text:"así que como podría Botas alcanzar al", time:100.5},
            {text:"pequeño y pobre Medias?", time:103}
        ],

        [
            {text:"Medias dio otro grito,", time:105},
            {text:"por favor desconecta el enchufe de la lavadora!", time:102 + 7},
            {text:"Eso es! Dice Botas, pero ¿dónde está el enchufe?", time:113.4},
            {text:"El busca por todas partes", time:116},
            {text:"inclusive debajo de la alfombra.", time:118.3}
        ],
        [
            {text:"Aquí esta! Empieza a gritar Botas.", time:118 + 3.6},
            {text:"El corre a la pared y arranca el enchufe", time:118 + 6.2},
            {text:"La lavadora se detiene y la puerta se abre.", time:118 + 11.8},
            {text:"Botas ha salvado a su mejor amigo Medias.", time:134}
        ],
        [

            {text:"Ahora medias esta seguro y aprendió hoy", time:138},
            {text:"que sólo debería jugar donde él sabe", time:141.5},
            {text:"que es seguro jugar.", time:144},
            {text:"Puedes pensar en lugares que", time:145},
            {text:"no son seguros para jugar?", time:147.1}

        ],
        [
        ]
    ]
];